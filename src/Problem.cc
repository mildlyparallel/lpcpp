#include <cassert>
#include <stdexcept>

#include "Problem.hh"

#include "ProblemException.hh"

#include <lpsolve/lp_lib.h>

namespace lpcpp {

static const int constr_type_map[Problem::nr_verbose_types] = {
	[Problem::Greater] = GE,
	[Problem::Less]    = LE,
	[Problem::Equal]   = EQ
};

static const int verbose_type_map[Problem::nr_verbose_types] = {
	[Problem::Neutral]   = NEUTRAL,
	[Problem::Critical]  = CRITICAL,
	[Problem::Severe]    = SEVERE,
	[Problem::Important] = IMPORTANT,
	[Problem::Normal]    = NORMAL,
	[Problem::Detailed]  = DETAILED,
	[Problem::Full]      = FULL
};

static const int result_type_map[Problem::nr_result_types] = {
	[Problem::UnknownError] = UNKNOWNERROR,
	[Problem::DataIgnored]  = DATAIGNORED,
	[Problem::NoBFP]        = NOBFP,
	[Problem::Nomemory]     = NOMEMORY,
	[Problem::NotRun]       = NOTRUN,
	[Problem::Optimal]      = OPTIMAL,
	[Problem::Suboptimal]   = SUBOPTIMAL,
	[Problem::Infeasible]   = INFEASIBLE,
	[Problem::Unbounded]    = UNBOUNDED,
	[Problem::Degenerate]   = DEGENERATE,
	[Problem::Numfailure]   = NUMFAILURE,
	[Problem::Userabort]    = USERABORT,
	[Problem::Timeout]      = TIMEOUT,
	[Problem::Running]      = RUNNING,
	[Problem::Presolved]    = PRESOLVED
};

Problem::Problem(size_t nr_vars)
: m_nr_vars(nr_vars)
, m_colno(nr_vars)
, m_row(nr_vars)
{
	m_lp = ::make_lp(0, nr_vars);
	if (!m_lp)
		throw ProblemException(ProblemException::InitFailed);

	for (size_t i = 0; i < nr_vars; ++i)
		m_colno[i] = i + 1;

	set_rowmode(true);
	
	fill_row(0);
}

Problem::~Problem()
{  
	assert(m_lp);
	::delete_lp(m_lp);
}

void Problem::set_rowmode(bool rowmode)
{
	assert(m_lp);
	::set_add_rowmode(m_lp, rowmode);
}

bool Problem::get_rowmode() const
{
	assert(m_lp);
	return ::is_add_rowmode(m_lp) == TRUE;
}

size_t Problem::nr_vars() const
{
	return m_nr_vars;
}

lprec *Problem::get_handle() const
{
	return m_lp;
}

double &Problem::operator[](size_t i)
{
	assert(i < m_row.size());
	return m_row[i];
}

double Problem::operator[](size_t i) const
{
	assert(i < m_row.size());
	return m_row[i];
}

void Problem::add_constraint(ConstraintType type, double rhs)
{
	assert(m_lp);
	assert(get_rowmode());

	int constr_type = constr_type_map[static_cast<size_t>(type)];

	int rc = ::add_constraintex(m_lp, m_nr_vars, m_row.data(), m_colno.data(), constr_type, rhs);
	if (!rc)
		throw ProblemException(ProblemException::AddConstraintFailed);
}

void Problem::set_unbounded(size_t i)
{
	assert(m_lp);
	assert(i < m_nr_vars);
	if (get_rowmode())
		set_rowmode(false);

	::set_unbounded(m_lp, m_colno[i]);
}

void Problem::set_bound_lower(size_t i, double lo)
{
	assert(m_lp);
	assert(i < m_nr_vars);
	if (get_rowmode())
		set_rowmode(false);

	::set_lowbo(m_lp, m_colno[i], lo);
}

void Problem::set_bound_upper(size_t i, double up)
{
	assert(m_lp);
	assert(i < m_nr_vars);
	if (get_rowmode())
		set_rowmode(false);

	::set_upbo(m_lp, m_colno[i], up);
}

void Problem::set_bound_lower_upper(size_t i, double lo, double up)
{
	assert(m_lp);
	assert(i < m_nr_vars);
	if (get_rowmode())
		set_rowmode(false);

	::set_bounds(m_lp, m_colno[i], lo, up);
}

void Problem::set_bounds(size_t i, std::optional<double> lo, std::optional<double> up)
{
	if (lo && up) {
		set_bound_lower_upper(i, lo.value(), up.value());
	} else if (lo) {
		set_unbounded(i);
		set_bound_lower(i, lo.value());
	} else if (up) {
		set_unbounded(i);
		set_bound_upper(i, up.value());
	} else {
		set_unbounded(i);
	}
}

void Problem::set_unbounded_all()
{
	for (size_t i = 0; i < m_nr_vars; ++i)
		set_unbounded(i);
}

void Problem::set_bound_lower_all(double lo)
{
	for (size_t i = 0; i < m_nr_vars; ++i)
		set_bound_lower(i, lo);
}

void Problem::set_bound_upper_all(double up)
{
	for (size_t i = 0; i < m_nr_vars; ++i)
		set_bound_upper(i, up);
}

void Problem::set_bound_lower_upper_all(double lo, double up)
{
	for (size_t i = 0; i < m_nr_vars; ++i) {
		set_bound_lower_upper(i, lo, up);
	}
}

void Problem::set_bounds_all(std::optional<double> lo, std::optional<double> up)
{
	for (size_t i = 0; i < m_nr_vars; ++i) {
		set_bounds(i, lo, up);
	}
}

void Problem::set_objective_minim()
{
	assert(m_lp);
	if (get_rowmode())
		set_rowmode(false);

	::set_minim(m_lp);

	int rc = ::set_obj_fnex(m_lp, m_nr_vars, m_row.data(), m_colno.data());
	if (!rc)
		throw ProblemException(ProblemException::SetObjectiveFailed);
}

void Problem::set_objective_maxim()
{
	assert(m_lp);
	if (get_rowmode())
		set_rowmode(false);

	::set_maxim(m_lp);

	int rc = ::set_obj_fnex(m_lp, m_nr_vars, m_row.data(), m_colno.data());
	if (!rc)
		throw ProblemException(ProblemException::SetObjectiveFailed);
}

void Problem::set_verbose(VerboseType type)
{
	::set_verbose(m_lp, verbose_type_map[static_cast<size_t>(type)]);
}

Problem::ResultType Problem::solve()
{
	if (get_rowmode())
		set_rowmode(false);

	int rc = ::solve(m_lp);

	for (size_t i = 0; i < nr_result_types; ++i) {
		if (result_type_map[i] == rc)
			return static_cast<ResultType>(i);
	}

	assert(false);
	return UnknownError;
}

double Problem::optimize()
{
	ResultType res = solve();
	if (res != Optimal)
		throw ProblemException(ProblemException::NoSolution);

	::get_variables(m_lp, m_row.data());

	return get_objective();
}

double Problem::get_objective() const
{
	return ::get_objective(m_lp);
}

void Problem::fill_row(double value)
{
	std::fill(m_row.begin(), m_row.end(), value);
}

void Problem::fill_row(std::initializer_list<double> row)
{
	assert(row.size() == nr_vars());

	size_t j = 0;
	for (auto i = row.begin(); i != row.end(); ++i) {
		m_row[j] = *i;
		j++;
		if (j == m_nr_vars)
			break;
	}
}

const std::vector<double> &Problem::get_row() const
{
	return m_row;
}

void Problem::print_lp() const
{
	::print_lp(m_lp);
}

void Problem::print_tableau() const
{
	::print_tableau(m_lp);
}

void Problem::print_objective() const
{
	::print_objective(m_lp);
}

void Problem::print_duals() const
{
	::print_duals(m_lp);
}

} /* namespace lpcpp */

