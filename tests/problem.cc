#include "gtest/gtest.h"

#include "Problem.hh"

using namespace ::testing;
using namespace lpcpp;

#define EPS 1e-6

TEST(TestProblem, test_1) {
	Problem lp(3);
	lp.set_verbose(Problem::Important);
	ASSERT_EQ(lp.nr_vars(), 3);
	ASSERT_TRUE(lp.get_handle());

	lp.fill_row({2, 1, 1});
	lp.add_constraint(Problem::Less, 150);

	lp.fill_row({2, 2, 8});
	lp.add_constraint(Problem::Less, 200);

	lp[0] = 2;
	lp[1] = 3;
	lp[2] = 1;
	lp.add_constraint(Problem::Less, 320);

	lp.fill_row({3, 2, 1});
	lp.set_objective_maxim();

	lp.set_bounds_all(-1, 70);

	double opt = lp.optimize();

	std::cout << "opt = " << opt << std::endl;

	std::cout << "==== print_lp =====" << std::endl;
	lp.print_lp();
	std::cout << "==== print_duals =====" << std::endl;
	lp.print_duals();
	std::cout << "==== print_objective =====" << std::endl;
	lp.print_objective();
	std::cout << "==== print_tableau =====" << std::endl;
	lp.print_tableau();

	EXPECT_NEAR(opt, 254, EPS);
	EXPECT_NEAR(lp[0], 47, EPS);
	EXPECT_NEAR(lp[1], 57, EPS);
	EXPECT_NEAR(lp[2], -1, EPS);
}

