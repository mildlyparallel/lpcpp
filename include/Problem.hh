#pragma once

#include <vector>
#include <initializer_list>
#include <optional>

struct _lprec;
typedef struct _lprec lprec;

namespace lpcpp {


class Problem
{
public:
	Problem(size_t nr_vars);

	~Problem();

	size_t nr_vars() const;

	lprec *get_handle() const;

	enum ConstraintType {
		Greater = 0,
		Less,
		Equal,

		nr_constr_types
	};

	void add_constraint(ConstraintType type, double rhs);

	void set_objective_minim();

	void set_objective_maxim();

	double &operator[](size_t i);

	double operator[](size_t i) const;

	void fill_row(std::initializer_list<double> row);

	void fill_row(double value);

	const std::vector<double> &get_row() const;

	enum VerboseType {
		Neutral = 0,
		Critical,
		Severe,
		Important,
		Normal,
		Detailed,
		Full,

		nr_verbose_types
	};

	void set_verbose(VerboseType type);

	enum ResultType {
		UnknownError = 0,
		DataIgnored,
		NoBFP,
		Nomemory,
		NotRun,
		Optimal,
		Suboptimal,
		Infeasible,
		Unbounded,
		Degenerate,
		Numfailure,
		Userabort,
		Timeout,
		Running,
		Presolved,

		nr_result_types
	};

	void set_unbounded(size_t i);
	void set_bound_lower(size_t i, double lo);
	void set_bound_upper(size_t i, double up);
	void set_bound_lower_upper(size_t i, double lo, double up);
	void set_bounds(size_t i, std::optional<double> lo, std::optional<double> up);

	void set_unbounded_all();
	void set_bound_lower_all(double lo);
	void set_bound_upper_all(double up);
	void set_bound_lower_upper_all(double lo, double up);
	void set_bounds_all(std::optional<double> lo, std::optional<double> up);

	ResultType solve();

	double optimize();

	double get_objective() const;

	void print_lp() const;
	void print_tableau() const;
	void print_objective() const;
	void print_duals() const;

protected:

private:
	bool get_rowmode() const;

	void set_rowmode(bool rowmode);

	size_t m_nr_vars;

	lprec *m_lp;

	std::vector<int> m_colno;

	std::vector<double> m_row;
};

} /* namespace lpcpp */
