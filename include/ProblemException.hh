#pragma once

#include <stdexcept>

class ProblemException : public std::exception
{
public:
	enum ErrorType {
		Unknown = 0,
		InitFailed,
		AddConstraintFailed,
		SetObjectiveFailed,
		NoSolution,

		nr_types
	};

	ProblemException(ErrorType err)
	: m_error(err)
	{  }

	virtual const char *what() const noexcept
	{
		return m_messages[static_cast<size_t>(m_error)];
	}

protected:

private:
	ErrorType m_error;

	static constexpr const char *m_messages[nr_types] = {
		[Unknown]             = "Unknwon error",
		[InitFailed]          = "lpsolve initialization failed",
		[AddConstraintFailed] = "Error during adding constraint",
		[SetObjectiveFailed]  = "Error during setting objective function",
		[NoSolution]          = "Optimal solution is not found",
	};
};

